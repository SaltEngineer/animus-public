# Animus Public

Placeholder for some documentation

## Serverside ESP compensation
The easiest way to determine if a player is observing an AActor would be to utilize existing culling systems by referencing the player's FSceneView. Unfortunately, while the method is accurate in determining all specified AActors within the current FOV, this depends on being able to access the local player controller. As a result, it would require trusting a client, and clients are not to be trusted.

Leveraging other methods, however, we are able to get other actors residing within a players FOV - as determined by the server. Doing analysis at this point is relatively simple, as we simply need to perform basic visibility checks to make a determination.

![](images/VisibleActors.png)

Above we have an example of perfoming observation checks against the base character class in the Animus plugin. The test actor in this instance is a child AI class, and as a result is detected in the exact same way a player class would be.

![](images/ObservingNonVisibleActors.png)

When we relocate and attempt to observe the actor through a wall, we're provided with feedback determining that the player is potentially observing the actor, and that they do not actually have visibility to any part of the underlying model.

![](images/IndirectObservationDetection.png)

Additionally, this is not dependent on line traces. As a result, we are able to also determine if a player is making indirect observations on other parts of the screen. The X in the center highlights the center of the test window.

### Data collection
The data on actor visibility and observation status is enriched data points including timestamps of status changes, and other relevant gameplay information such as distance and current actions. With the enhanced data we are able to make a determination of whether the player is interacting with another actor outside of normal expectations. As an example, if the target actor has never been visible and is out of earshot range, there should be no reasonable knowlege of it's position.

This data can be further combined with player locations and be utilized to create post-game playbacks of key events for automated/manual review.